package realMonteCarloJavaSockets;

public class Main {

	public static void main(String[] args) {
	    
		String host = "127.0.0.";
		String hosts[] = new String[6];
  
		for(int i = 1; i<=hosts.length;i++) hosts[i-1]="127.0.0."+i;
  
		int port = 2345;
		Integer[] ports = new Integer[6];
		for(int i = 0; i<ports.length;i++) ports[i]=2345+i;
   
		CreateServers ts1 = new CreateServers(hosts[0], ports[0]);
		CreateServers ts2 = new CreateServers(hosts[1], ports[1]);
		CreateServers ts3 = new CreateServers(hosts[2], ports[2]);
		CreateServers ts4 = new CreateServers(hosts[3], ports[3]);
		CreateServers ts5 = new CreateServers(hosts[4], ports[4]);
		CreateServers ts6 = new CreateServers(hosts[5], ports[5]);

		ts1.open();
		ts2.open();
		ts3.open();
		ts4.open();
		ts5.open();
		ts6.open();
  
		System.out.println("Serveurs initialisÚs.");
		Thread t = new Thread (new Client(hosts, ports, 16000000));
		t.start();
		}
}