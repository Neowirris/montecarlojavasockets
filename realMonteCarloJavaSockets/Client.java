package realMonteCarloJavaSockets;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client implements Runnable{

	private Socket connexion = null;
	private PrintWriter writer = null;
	private BufferedInputStream reader = null;
	private String name = "Client-";
	private String[] hosts = null;
	private Integer[] ports = null;
	private long resultat = 0;
	private int nbIterations = 0;
	
	 public Client(String host, int port, int nbIterations){
		 this.nbIterations = nbIterations;
		 try {
	         connexion = new Socket(host, port);
	      } catch (UnknownHostException e) {
	         e.printStackTrace();
	      } catch (IOException e) {
	         e.printStackTrace();
	      }
	   }
	 
	 public Client(String[] hosts, Integer[] ports, int nbIterations) {
		 this.hosts = hosts;
		 this.ports = ports;
		 this.nbIterations = nbIterations;
	 }
	 
	 public void run() {
		 if(connexion == null) {
			 for(int i = 0; i<hosts.length; i++) {
				 try {
			         connexion = new Socket(hosts[i], ports[i]);
			      } catch (UnknownHostException e) {
			         e.printStackTrace();
			      } catch (IOException e) {
			         e.printStackTrace();
			      }
//				 try {
//					Thread.currentThread().sleep(100);
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
				 try {
					 writer = new PrintWriter(connexion.getOutputStream(), true);
			            reader = new BufferedInputStream(connexion.getInputStream());
			            //On envoie la commande au serveur
			            
			            String commande = Integer.toString(nbIterations);
			            writer.write(commande);
			            //TOUJOURS UTILISER flush() POUR ENVOYER R�ELLEMENT DES INFOS AU SERVEUR
			            writer.flush();
			            
			            //On attend la r�ponse
			            String response = read();
			            resultat += Integer.parseInt(response);
			            
			            
			         } catch (IOException e1) {
			            e1.printStackTrace();
			         }
			         
//			         try {
//			            Thread.currentThread().sleep(100);
//			         } catch (InterruptedException e) {
//			        	 e.printStackTrace();
//			         }
//				     
			         writer.write("CLOSE");
				     writer.flush();
//					 System.out.println("Commande CLOSE envoy�e au serveur");
				     writer.close();
			 }
		 }
		 else {
//			 try {
//				Thread.currentThread().sleep(100);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
			 try {
				 writer = new PrintWriter(connexion.getOutputStream(), true);
		            reader = new BufferedInputStream(connexion.getInputStream());
		            //On envoie la commande au serveur
		            
		            String commande = Integer.toString(nbIterations);
		            writer.write(commande);
		            //TOUJOURS UTILISER flush() POUR ENVOYER R�ELLEMENT DES INFOS AU SERVEUR
		            writer.flush();  
		            
//			        System.out.println("Commande " + commande + " envoy�e au serveur");
		            
		            //On attend la r�ponse
		            String response = read();
		            resultat += Integer.parseInt(response);
//			        System.out.println("\t * " + name + " : R�ponse re�ue " + response);
		            
		         } catch (IOException e1) {
		            e1.printStackTrace();
		         }
		         
//		         try {
//		            Thread.currentThread().sleep(100);
//		         } catch (InterruptedException e) {
//		            e.printStackTrace();
//		         }
			      
			      writer.write("CLOSE");
			      writer.flush();
			      writer.close();
		 }
		 	
		 System.out.println("R�sultat de l'ex�cution : ");
		 double pi = 4.0 * resultat /hosts.length/nbIterations;
		 System.out.println("Pi : "+pi);
		 System.out.println("Diff�rence avec la valeur exacte de Pi : "+(pi-Math.PI));
		 System.out.println("Erreur : "+(pi - Math.PI)/Math.PI * 100 + " %");
		 
	 }
		   //M�thode pour lire les r�ponses du serveur
		   private String read() throws IOException{      
		      String response = "";
		      int stream;
		      byte[] b = new byte[4096];
		      stream = reader.read(b);
		      response = new String(b, 0, stream);      
		      return response;
		   }   
		}