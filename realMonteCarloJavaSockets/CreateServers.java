package realMonteCarloJavaSockets;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class CreateServers {

	//On initialise des valeurs par d�faut
	private int port = 2345;
	private String host = "127.0.0.1";
	private ServerSocket server = null;
	private boolean isRunning = true;
	private int nbServeurs;
	
	public CreateServers () {
			try {
				server = new ServerSocket(port, 5, InetAddress.getByName(host));
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public CreateServers (String pHost, int pPort) {
		host = pHost;
		port = pPort;
		
			try {
				server = new ServerSocket(port, 5, InetAddress.getByName(host));
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	//On lance le serveur
	public void open() {
		Thread t = new Thread(new Runnable() {
	
			@Override
			public void run() {
				while(isRunning == true) {
					try {
						Socket client = server.accept();
//						System.out.println("Connexion cliente re�ue. Serveur "+server.getInetAddress().getHostAddress());
						Thread t = new Thread(new ClientHandler(client));
						t.start();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				try {
					server.close();
					isRunning = false;
				} catch (IOException e) {
					e.printStackTrace();
					server = null;
				}
			}
		
		});
		t.start();
	}   
}