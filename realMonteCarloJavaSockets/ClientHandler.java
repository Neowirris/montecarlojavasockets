package realMonteCarloJavaSockets;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.Random;

public class ClientHandler implements Runnable{

	private Socket sock;
	private PrintWriter writer = null;
	private BufferedInputStream reader = null;
	
	public ClientHandler(Socket pSock) {
		sock = pSock;
	}
	   
	@Override
	//Traitement lanc� d'un un thread s�par�
	public void run() {
		boolean closeConnexion = false;
		//Tant que la connexion est active, on traite les demandes
		while(!sock.isClosed()) {
			try {
				writer = new PrintWriter(sock.getOutputStream());
				reader = new BufferedInputStream(sock.getInputStream());

				String response = read();
				if (response != null) {
					//String de debug
//					InetSocketAddress remote = (InetSocketAddress)sock.getRemoteSocketAddress();
//					String debug = "";
//					debug = "Thread : " + Thread.currentThread().getName() + ". ";
//					debug += "Demande de l'adresse : " + remote.getAddress().getHostAddress() +".";
//					debug += " Sur le port : " + remote.getPort() + ".\n";
//					debug += "\t -> Commande re�ue : " + response + "\n";
//					System.err.println("\n" + debug);
					if(!response.equals("CLOSE")) {
						String toSend = "";
						long circleCount = 0;
					    Random prng = new Random ();
					    for (int j = 0; j < Integer.parseInt(response); j++)
					    {
					      double x = prng.nextDouble();
					      double y = prng.nextDouble();
					      if ((x * x + y * y) < 1)  ++circleCount;
					    }
					    
					    toSend = Long.toString(circleCount);
//							On envoie la r�ponse au client
						writer.write(toSend);
//							Utilise flush pour envoyer les donn�es au client
						writer.flush();
					}
				}
				else closeConnexion = true;

			}catch(SocketException e){
				System.err.println("LA CONNEXION A ETE INTERROMPUE ! ");
				break;
			} catch (IOException e) {
				e.printStackTrace();
			}
//			Si la commande de fermeture de connexion a �t� d�tect�e
			if(closeConnexion){
//				System.err.println("COMMANDE CLOSE DETECTEE ! ");
				writer = null;
				reader = null;
				try {
					sock.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			}
		}
		
	}
	public String read() throws IOException{
		String response = "";
		int stream;
		byte[] b = new byte[4096];
		stream = reader.read(b);
		if(stream > -1) response = new String(b,0,stream);
		else response = null;
		return response;
		
	}
}